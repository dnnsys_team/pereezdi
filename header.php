<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pereezdi
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
<!--	<a class="skip-link screen-reader-text" href="#content">--><?php //esc_html_e( 'Skip to content', 'pereezdi' ); ?><!--</a>-->

<!--	<header id="masthead" class="site-header">-->
<!--		<div class="site-branding">-->
<!--			--><?php
//			the_custom_logo();
//			if ( is_front_page() && is_home() ) : ?>
<!--				<h1 class="site-title"><a href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" rel="home">--><?php //bloginfo( 'name' ); ?><!--</a></h1>-->
<!--			--><?php //else : ?>
<!--				<p class="site-title"><a href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" rel="home">--><?php //bloginfo( 'name' ); ?><!--</a></p>-->
<!--			--><?php
//			endif;
//
//			$description = get_bloginfo( 'description', 'display' );
//			if ( $description || is_customize_preview() ) : ?>
<!--				<p class="site-description">--><?php //echo $description; /* WPCS: xss ok. */ ?><!--</p>-->
<!--			--><?php
//			endif; ?>
<!--		</div><!-- .site-branding -->
<!---->
<!--		<nav id="site-navigation" class="main-navigation">-->
<!--			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">--><?php //esc_html_e( 'Primary Menu', 'pereezdi' ); ?><!--</button>-->
<!--			--><?php
//				wp_nav_menu( array(
//					'theme_location' => 'menu-1',
//					'menu_id'        => 'primary-menu',
//				) );
//			?>
<!--		</nav><!-- #site-navigation -->
<!--	</header><!-- #masthead -->

    <heder class="header">
        <div class="container">
            <div class="header__logo header__element">
                <?php
                $custom_logo = the_custom_logo();
                if (isset($custom_logo)) {
                    echo $custom_logo;
                } else {
                    echo '<img src="' . get_template_directory_uri() . '/build/images/logo.png" alt="Переезды">';
                }
                ?>
            </div>
            <div class="header__menu header__element">
                <ul class="header__nav">
                    <li>
                        <a href="#" class="header__nav-item">Главная</a>
                    </li>
                    <li>
                        <a href="#" class="header__nav-item">Для бизнеса</a>
                    </li>
                    <li>
                        <a href="#" class="header__nav-item">Частным лицам</a>
                    </li>
                    <li>
                        <a href="#" class="header__nav-item">Новости</a>
                    </li>
                    <li>
                        <a href="#" class="header__nav-item">Контакты</a>
                    </li>
                </ul>
            </div>

        </div>
    </heder>

	<div class="container">
