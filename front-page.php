<?php get_header() ?>
    <div class="main-banner" style="background-image: url(<?php echo get_template_directory_uri() . '/build/images/main-banner.jpg'?>);">
        <div class="container">
            <div class="row">
                <div class="column-3">
                    <div class="row"><h2 class="main-banner__title">Собираетесь переезжать?</h2>
                        <div class="main-banner__text">
                            <p>Доверьте свой переезд команде профессионалов</p>
                            <ul class="list-with-mark">
                                <li>Мы бережно упакуем ваши вещи</li>
                                <li>Погрузим и разгрузим все машины</li>
                                <li>И даже расставим все на свои места!</li>
                            </ul>
                            <div class="main-banner__watch-link">
                                <span class="small">Посмотрите короткое видео о нашей работе</span><br/>
                                <a href="#"><span>Смотреть видео</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-about">
        <div class="block-about__element block-about__description dark-bg">
            <h2 class="dark-bg">Переезд с гарантиями!</h2>
            <p>Мы осуществляем комплексные переезды для частных жилищ, квартир и офисов.</p>
            <p>В наши услуги входит широкий спект возможностей. Бережная упаковка и погрузка всех ваших вещей.
                Транспортировка до места назначения и разгрузка в указанную область. Также наша компания предоставляет склад временного хранения для вещей и комплекс услуг по переезду офисов и бизнеса</p>
        </div>
        <div class="block-about__element block-about__image">
            <img src="<?php echo get_template_directory_uri() . '/build/images/about-block.jpg' ?>" alt="Переезд">
        </div>
        <div class="block-about__button">
            <a href="#" class="base-button">Заказать звонок</a>
        </div>
    </div>
    <div class="block-services">
        <div class="row">
            <div class="block-services__item">
                <div class="block-services__item-icon">
                    <img src="<?php echo get_template_directory_uri() . '/build/images/icons/1.png' ?>" alt="">
                </div>
                <div class="block-services__item-title">
                    Комплексный<br/>
                    квартирный переезд
                </div>
            </div>
            <div class="block-services__item">
                <div class="block-services__item-icon">
                    <img src="<?php echo get_template_directory_uri() . '/build/images/icons/2.png' ?>" alt="">
                </div>
                <div class="block-services__item-title">
                    Переезд для офисов<br/>
                    и для бизнеса
                </div>
            </div>
            <div class="block-services__item">
                <div class="block-services__item-icon">
                    <img src="<?php echo get_template_directory_uri() . '/build/images/icons/3.png' ?>" alt="">
                </div>
                <div class="block-services__item-title">
                    Переезд для частных<br/>
                    домов и коттеджей
                </div>
            </div>
            <div class="block-services__item">
                <div class="block-services__item-icon">
                    <img src="<?php echo get_template_directory_uri() . '/build/images/icons/4.png' ?>" alt="">
                </div>
                <div class="block-services__item-title">
                    Упаковка вещей и<br/>
                    услуги грузчиков
                </div>
            </div>
            <div class="block-services__item">
                <div class="block-services__item-icon">
                    <img src="<?php echo get_template_directory_uri() . '/build/images/icons/5.png' ?>" alt="">
                </div>
                <div class="block-services__item-title">
                    Предоставляем склад<br/>
                    временного хранения
                </div>
            </div>
        </div>
    </div>
    <div class="block-statistics white-background">
        <div class="row">
            <div class="block-statistics__item">
                <div class="block-statistics__item-count">
                    5
                </div>
                <div class="block-statistics__item-title">
                    лет опыта<br/>работы
                </div>
            </div>
            <div class="block-statistics__item">
                <div class="block-statistics__item-count">
                    450
                </div>
                <div class="block-statistics__item-title">
                    довольных<br/>клиентов
                </div>
            </div>
            <div class="block-statistics__item">
                <div class="block-statistics__item-count">
                    6
                </div>
                <div class="block-statistics__item-title">
                   собственных<br/>складов компании
                </div>
            </div>
            <div class="block-statistics__item">
                <div class="block-statistics__item-count">
                    24
                </div>
                <div class="block-statistics__item-title">
                    машин для<br/>перевозки
                </div>
            </div>
            <div class="block-statistics__item">
                <div class="block-statistics__item-count">
                    32
                </div>
                <div class="block-statistics__item-title">
                    сотрудника<br/>в штате компании
                </div>
            </div>
            <div class="block-statistics__item">
                <div class="block-statistics__item-count">
                    5
                </div>
                <div class="block-statistics__item-title">
                    различных<br/>способов оплаты
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>
